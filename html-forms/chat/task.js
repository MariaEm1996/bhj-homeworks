const MESSAGES = [
    'Закажи, прежде чем справшивать!',
    'А может ты уйдешь?',
    '!#%$',
];

document.addEventListener('DOMContentLoaded', () => {
    let isOpenedChat = false;

    const openChatButton = document.querySelector('.chat-widget');

    openChatButton.addEventListener('click', () => {
        openChatButton.classList.add('chat-widget_active');
        isOpenedChat = true;
    });

    const input = document.getElementById('chat-widget__input');
    const messagesContainer = document.getElementById('chat-widget__messages');

    document.addEventListener('keyup', (e) => {
        if (e.key !== 'Enter' || input.value.length === 0 || !isOpenedChat) {
            return;
        }

        messagesContainer.innerHTML += getMessageMarkup(input.value, true);
        messagesContainer.innerHTML += getMessageMarkup(MESSAGES[Math.floor(Math.random() * MESSAGES.length)]);

        input.value = '';
    });

    function getMessageMarkup(text, isUser = false) {
        const date = new Date();
        return `
            <div class="message ${isUser ? 'message_client' : ''}">
                <div class="message__time">${prettifyDate(date.getHours())}:${prettifyDate(date.getMinutes())}</div>
                <div class="message__text">${text}</div>
            </div>
        `;
    }

    function prettifyDate(datePart) {
        return datePart < 10 ? `0${datePart}` : datePart;
    }
});