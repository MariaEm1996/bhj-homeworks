document.addEventListener('DOMContentLoaded', () => {
    const inputs = Array.from(document.querySelectorAll('.interest__check'));

    inputs.forEach((input) => {
        input.addEventListener('change', () => {
            const children = Array.from(input.closest('.interest').querySelector('.interests_active')?.querySelectorAll('.interests_active .interest__check') ?? []);
            children.forEach(child => child.checked = !child.checked);
        });
    });
});