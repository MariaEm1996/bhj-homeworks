document.addEventListener('DOMContentLoaded', () => {
    const menu = new Menu();
    menu.init();
});
    

class Menu {
    constructor(root = document.body) {
        this.menuItemElements = Array.from(root.querySelectorAll('.menu__link'));
        this.openedSubmenuItem = null;
    }

    init() {
        this.menuItemElements.forEach((menuItemElement) => {
            const menuItem = new MenuItem(menuItemElement);
            menuItem.init();
            menuItem.onSubMenuOpen(() => {
                this.onSubMenuOpen(menuItem);
            });
        });
    }

    onSubMenuOpen(submenuToOpen) {
        if (this.openedSubmenuItem) {
            this.openedSubmenuItem.closeSub();
        }

        submenuToOpen.openSub();

        this.openedSubmenuItem = submenuToOpen;
    }
}

class MenuItem {
    constructor(element) {
        this.element = element;
        this.subMenu = this.element.closest('.menu__item').querySelector('.menu_sub');
        this.openCallbacks = [];
    }

    init() {
        if (!this.subMenu) {
            return;
        }

        this.element.addEventListener('click', (e) => {
            e.preventDefault();
            this.runSubmenuCallbacks();
        });
    }

    onSubMenuOpen(callback) {
        this.openCallbacks.push(callback);
    }

    runSubmenuCallbacks() {
        this.openCallbacks.forEach(callback => callback());
    }

    openSub() {
        this.subMenu.classList.add('menu_active');
    }

    closeSub() {
        this.subMenu.classList.remove('menu_active');
    }
}