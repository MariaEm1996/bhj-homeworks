document.addEventListener('DOMContentLoaded', () => {
    const prevButton = document.querySelector('.slider__arrow_prev');
    const nextButton = document.querySelector('.slider__arrow_next');
    const slides = Array.from(document.querySelectorAll('.slider__item'));
    const dots = Array.from(document.querySelectorAll('.slider__dot'));

    const slidesCount = slides.length;

    let currentSlidePosition = 0;

    prevButton.addEventListener('click', () => slideImage(-1));
    nextButton.addEventListener('click', () => slideImage(1));
    dots.forEach((dot, index) => {
        dot.addEventListener('click', () => setPostion(index));
    })

    function slideImage(offset) {
        deactivateSlide(currentSlidePosition);
        currentSlidePosition = (currentSlidePosition + offset + slidesCount) % slidesCount;
        activateSlide(currentSlidePosition);
    }

    function setPostion(position) {
        deactivateSlide(currentSlidePosition);
        currentSlidePosition = position
        activateSlide(currentSlidePosition);
    }

    function activateSlide(position) {
        slides[position].classList.add('slider__item_active');
        dots[position].classList.add('slider__dot_active');
    }

    function deactivateSlide(position) {
        slides[position].classList.remove('slider__item_active');
        dots[position].classList.remove('slider__dot_active');
    }
});