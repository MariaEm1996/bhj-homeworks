document.addEventListener('DOMContentLoaded', () => {
    const modal = new AcionModal('modal_main');

    modal.init();

    modal.show();
});

class Modal {
    constructor(modalId) {
        this.modal = document.getElementById(modalId);
        this.closeButtons = Array.from(this.modal.querySelectorAll('.modal__close'));
    }

    init() {
        this.closeButtons.forEach((closeButton) => {
            closeButton.addEventListener('click', () => this.hide());
        });
    }

    show() {
        this.modal.classList.add('modal_active');
    }

    hide() {
        this.modal.classList.remove('modal_active');
    }
}

class AcionModal extends Modal {
    constructor(modalId) {
        super(modalId);
        this.actionButtons = Array.from(this.modal.querySelectorAll('.show-success'));
        this.successModal = new Modal('modal_success');
    }

    init() {
        super.init();
        this.successModal.init();
        this.actionButtons.forEach((actionButton) => {
            actionButton.addEventListener('click', () => {
                this.hide();
                this.successModal.show();
            });
        });
    }
}