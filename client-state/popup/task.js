document.addEventListener('DOMContentLoaded', () => {
    const isShownPopup = Boolean(getCookie('shownPopup'));

    if (isShownPopup) {
        return;
    }

    const modal = new Modal('subscribe-modal');
    modal.init();
    modal.show();

    setCookie('shownPopup', 1);
});

class Modal {
    constructor(modalId) {
        this.modal = document.getElementById(modalId);
        this.closeButtons = Array.from(this.modal.querySelectorAll('.modal__close'));
    }

    init() {
        this.closeButtons.forEach((closeButton) => {
            closeButton.addEventListener('click', () => this.hide());
        });
    }

    show() {
        this.modal.classList.add('modal_active');
    }

    hide() {
        this.modal.classList.remove('modal_active');
    }
}

function setCookie(name, value) {
    document.cookie = `${name}=${value};`;
}

function getCookie(name) {
    const cookies = document.cookie.split('; ');

    for(let i = 0; i < cookies.length; i++) {
        const [cookieName, value] = cookies[i].split('=');
        if (cookieName === name) {
            return value;
        }
    }

    return null;
}