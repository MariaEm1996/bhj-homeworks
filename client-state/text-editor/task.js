document.addEventListener('DOMContentLoaded', () => {
    const textarea = document.getElementById('editor');

    if (localStorage.key('editor:data')) {
        textarea.value = localStorage.getItem('editor:data');
    }

    textarea.addEventListener('input', () => localStorage.setItem('editor:data', textarea.value));

    const clearEditorButton = document.getElementById('clear_editor');
    
    clearEditorButton.addEventListener('click', () => {
        textarea.value = '';
        localStorage.removeItem('editor:data');    
    });
});