document.addEventListener('DOMContentLoaded', () => {
    if (localStorage.key('userId')) {
        setUserId(localStorage.getItem('userId'));
        showWelcome();
        return;
    }

    const form = document.getElementById('signin__form');

    form.addEventListener('submit', (e) => {
        e.preventDefault();
        sendData(createFormData(form), auth);
    });

    function createFormData(form) {
        return new FormData(form);
    }

    function sendData(data, onReadySateChngeCallback) {
        const xhr = new XMLHttpRequest();

        xhr.open('post', 'https://netology-slow-rest.herokuapp.com/auth.php');

        xhr.onreadystatechange = function () {
            if (this.readyState !== xhr.DONE) {
                return;
            }

            onReadySateChngeCallback(JSON.parse(this.responseText));
        };

        xhr.send(data);
    }

    function auth(data) {
        if (!data.success) {
            alert('Неверный логин/пароль');
            return;
        }

        setUserId(data.user_id);
        showWelcome();
    }

    function setUserId(userId) {
        localStorage.setItem('userId', userId);
        const userIdInWelcomeBlock = document.getElementById('user_id');
        userIdInWelcomeBlock.innerText = userId;
    }

    function showWelcome() {
        const welcomeBlock = document.getElementById('welcome');
        welcomeBlock.classList.add('welcome_active');
    }
});